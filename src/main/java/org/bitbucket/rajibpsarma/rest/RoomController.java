package org.bitbucket.rajibpsarma.rest;

import org.bitbucket.rajibpsarma.data.entity.Room;
import org.bitbucket.rajibpsarma.data.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rooms")
public class RoomController {
	@Autowired
	private RoomRepository repository;
	
	@GetMapping()
	public Iterable<Room> getAllRooms() {
		/*
		List<Room> rooms = new ArrayList<Room>();
		rooms.add(new Room(101, "Room 101", "Single room with attached Bath"));
		rooms.add(new Room(201, "Room 201", "Double room with attached Bath"));
		return rooms;
		*/
		return repository.findAll();
	}
}
