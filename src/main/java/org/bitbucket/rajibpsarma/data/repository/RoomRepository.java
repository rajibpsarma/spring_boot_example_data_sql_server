package org.bitbucket.rajibpsarma.data.repository;

import org.bitbucket.rajibpsarma.data.entity.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {

}
