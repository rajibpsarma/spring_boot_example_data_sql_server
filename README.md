# Read data from SQL Server database #

 In this application, schema.sql and data.sql files are used to create and populate the ROOMS table,
 in SQL server.
 When we invoke the REST by using url: http://localhost:8080/rooms,
 it displays the data in JSON format:
  
[{
	"roomNumber": 101,
	"roomName": "Room 101",
	"roomDescription": "Single room with attached bath"
}, {
	"roomNumber": 201,
	"roomName": "Room 201",
	"roomDescription": "Double room with attached bath"
}]